const child_process = require('child_process');
const fs = require("fs");
const path = require("path");

const exec = (cmd) => {
    child_process.execSync(cmd, { stdio: 'inherit' });
}

fs.rmSync("./dist", { "recursive": true, force: true });

exec("npx tsc -m ES6")
fs.renameSync("dist/index.js", "dist/index.mjs")

exec("npx tsc -m CommonJS")
fs.renameSync("dist/index.js", "dist/index.cjs")
